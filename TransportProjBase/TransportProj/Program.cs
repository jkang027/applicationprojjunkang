﻿using Nito.AsyncEx;
using System;
using System.Collections.ObjectModel;
using TransportProj.Domain;
using TransportProj.Factory;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            //Establish factory and create city
            CarFactory factory = new HondaFactory();
            Random random = new Random();
            int CityLength = 10;
            int CityWidth = 10;
            City MyCity = new City(CityLength, CityWidth);

            //Establish how many buildings to add to city map
            int numberOfBuildings;
            while (true)
            {
                Console.WriteLine("How many buildings are in your city? (Please enter a number)");
                try
                {
                    numberOfBuildings = Int32.Parse(Console.ReadLine());
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid input. Please enter a number.");
                }
            }
            for (int i = 0; i < numberOfBuildings; i++)
            {
                var building = MyCity.AddBuildingToCity(random);
            }

            //Establish what kind of car you want
            Console.WriteLine("Would you like a \"Sedan,\" \"Race Car,\" or \"Super Car?\"");
            Car car = null;
            Passenger passenger = null;
            //Based on your decision, factory will create that type of car, add the car to MyCity, and add a new passenger to MyCity.
            while (car == null)
            {
                var carChoice = Console.ReadLine().ToLower();
                try
                {
                    car = factory.createCar(carChoice, MyCity.GetRandomEmptyCityPosition(random));
                    MyCity.AddCarToCity(car);
                    passenger = MyCity.AddPassengerToCity(random);
                }
                catch (NotSupportedException)
                {
                    Console.WriteLine("Invalid input. Please enter \"Sedan\", \"Race Car\", or \"Super Car\"");
                }
            }
            Console.WriteLine("Hit enter to call \"Tick\" method or type \"quit\" to exit.");
            while (true)
            {
                if (!passenger.Arrived)
                {
                    var input = Console.ReadLine().ToLower();
                    switch (input)
                    {
                        case "":
                            Tick(car, passenger);
                            //Everytime Tick is run, MainAsync will run to download veyo.com asyncronously
                            AsyncContext.Run(() => MainAsync(args));
                            break;
                        case "quit":
                            return;
                        default:
                            Console.WriteLine("Invalid input. Hit \"enter\" to call \"Tick\" method or type \"quit\" to exit.");
                            break;
                    }
                }
                else
                {
                    var input = Console.ReadLine().ToLower();
                    switch (input)
                    {
                        case "yes":
                            passenger = MyCity.AddPassengerToCity(random);
                            MyCity.BuildMap();
                            Console.WriteLine("Car is at (" + car.CityPosition.XPos + ", " + car.CityPosition.YPos + ")");
                            Console.WriteLine("New passenger is at (" + passenger.CityPosition.XPos + ", " + passenger.CityPosition.YPos + ").");
                            break;
                        case "no":
                            return;
                        default:
                            Console.WriteLine("Invalid input. Please type \"yes\" or \"no\"");
                            break;
                    }
                }
            }
        }

        static async void MainAsync(string[] args)
        {
            Downloader downloader = new Downloader();
            await downloader.Download();
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(Car car, Passenger passenger)
        {
            //Declare the status based on which conditional statement returns true
            var status = car.CityPosition.XPos != passenger.CityPosition.XPos ? "moveXPos" :
                         car.CityPosition.YPos != passenger.CityPosition.YPos ? "moveYPos" :
                         car.CityPosition.XPos == passenger.CityPosition.XPos && car.CityPosition.YPos == passenger.CityPosition.YPos && car.Passenger == null && passenger.Car == null ? "arrivedAtPassenger" :
                         car.CityPosition.XPos != passenger.DestinationXPos ? "moveToDestinationX" : 
                         car.CityPosition.YPos != passenger.DestinationYPos ? "moveToDestinationY" : "arrivedAtDestination";

            //Figure out which discrete action to take for each Tick based on the status. 
            switch (status)
            {
                case "moveXPos":
                    if (car.CityPosition.XPos < passenger.CityPosition.XPos)
                    {
                        car.MoveRight(passenger);
                    }
                    else
                    {
                        car.MoveLeft(passenger);
                    }
                    break;
                case "moveYPos":
                    if (car.CityPosition.YPos < passenger.CityPosition.YPos)
                    {
                        car.MoveUp(passenger);
                    }
                    else
                    {
                        car.MoveDown(passenger);
                    }
                    break;
                case "arrivedAtPassenger":
                    Console.WriteLine("Arrived at passenger's location.");
                    passenger.GetInCar(car);
                    break;
                case "moveToDestinationX":
                    if (car.CityPosition.XPos < passenger.DestinationXPos)
                    {
                        car.MoveRight(passenger);
                    }
                    else
                    {
                        car.MoveLeft(passenger);
                    }
                    break;
                case "moveToDestinationY":
                    if (car.CityPosition.YPos < passenger.DestinationYPos)
                    {
                        car.MoveUp(passenger);
                    }
                    else
                    {
                        car.MoveDown(passenger);
                    }
                    break;
                case "arrivedAtDestination":
                    Console.WriteLine("Arrived at passenger's destination.");
                    passenger.GetOutOfCar(car);
                    passenger.Arrived = true;
                    Console.WriteLine("Would you like another passenger? Type \"yes\" or \"no\"");
                    break;
                default:
                    Console.WriteLine("Something went wrong. Please restart the program.");
                    break;
            }

            /* This is how the Tick method was before. I know some people think ternary statements are more difficult to read and understand so I left the previous Tick version commented below. 
             * 
             * if (car.XPos != passenger.CityPosition.XPos)
             * {
             *     if (car.XPos < passenger.CityPosition.XPos)
             *     {
             *         car.MoveRight(passenger);
             *     }
             *     else
             *     {
             *         car.MoveLeft(passenger);
             *     }
             * }
             * else if (car.YPos != passenger.CityPosition.YPos)
             * {
             *     if (car.YPos < passenger.CityPosition.YPos)
             *     {
             *         car.MoveUp(passenger);
             *     }
             *     else
             *     {
             *         car.MoveDown(passenger);
             *     }
             * }
             * else if (car.XPos == passenger.CityPosition.XPos && car.YPos == passenger.CityPosition.YPos && car.Passenger == null && passenger.Car == null)
             * {
             *     Console.WriteLine("Arrived at passenger's location.");
             *     passenger.GetInCar(car);
             * }
             * else if (!passenger.IsAtDestination())
             * {
             *     if (car.XPos != passenger.DestinationXPos)
             *     {
             *         if (car.XPos < passenger.DestinationXPos)
             *         {
             *             car.MoveRight(passenger);
             *         }
             *         else
             *         {
             *             car.MoveLeft(passenger);
             *         }
             *     }
             *     else if (car.YPos != passenger.DestinationYPos)
             *     {
             *         if (car.YPos < passenger.DestinationYPos)
             *         {
             *             car.MoveUp(passenger);
             *         }
             *         else
             *         {
             *             car.MoveDown(passenger);
             *         }
             *     }
             * }
             * else if (passenger.IsAtDestination())
             * {
             *     Console.WriteLine("Arrived at passenger's destination.");
             *     passenger.GetOutOfCar(car);
             *     passenger.Arrived = true;
             * }
             */
        }
    }
}