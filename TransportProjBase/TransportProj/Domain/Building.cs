﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Domain
{
    public class Building
    {
        public CityPosition CityPosition { get; set; }

        public Building(CityPosition cityPosition)
        {
            CityPosition = cityPosition;
        }
    }
}
