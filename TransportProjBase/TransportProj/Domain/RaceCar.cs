﻿using System;

namespace TransportProj.Domain
{
    public class RaceCar : Car
    {
        public RaceCar(CityPosition cityPosition) : base(cityPosition)
        {
            Speed = 2;
        }
        
        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("RaceCar moved to ({0}, {1})", CityPosition.XPos, CityPosition.YPos));
        }
    }
}
