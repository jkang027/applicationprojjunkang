﻿using System;

namespace TransportProj.Domain
{
    public class SuperCar : Car
    {
        public SuperCar(CityPosition cityPosition) : base(cityPosition)
        {
            Speed = 3;
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("SuperCar moved to ({0}, {1})", CityPosition.XPos, CityPosition.YPos));
        }
    }
}
