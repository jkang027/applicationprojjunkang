﻿using System;
using System.Collections.ObjectModel;

namespace TransportProj.Domain
{
    public class Passenger
    {
        public int DestinationXPos { get; private set; }
        public int DestinationYPos { get; private set; }
        public bool Arrived { get; set; }

        public Car Car { get; set; }
        public CityPosition CityPosition { get; set; }

        public Passenger(int destXPos, int destYPos, CityPosition cityPosition)
        {
            DestinationXPos = destXPos;
            DestinationYPos = destYPos;
            Arrived = false;
            CityPosition = cityPosition;
        }

        public void GetInCar(Car car)
        {
            Car = car;
            car.PickupPassenger(this);
            CityPosition.City.BuildMap();
            Console.WriteLine("Passenger got in the car. Passenger is going to (" + this.DestinationXPos + ", " + this.DestinationYPos + ")");
        }

        public void GetOutOfCar(Car car)
        {
            car.DropOffPassenger();
            Car = null;
            CityPosition.RemovePassengerFromCity(this);
            Console.WriteLine("Passenger got out of the car.");
        }

        public bool IsAtDestination()
        {
            return CityPosition.XPos == DestinationXPos && CityPosition.YPos == DestinationYPos;
        }
    }
}
