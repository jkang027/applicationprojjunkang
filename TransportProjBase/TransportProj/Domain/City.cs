﻿using System;
using System.Collections.ObjectModel;

namespace TransportProj.Domain
{
    public class City
    {
        public static int YMax { get; private set; }
        public static int XMax { get; private set; }

        public Collection<Collection<CityPosition>> CityPositions { get; set; }

        public City(int xMax, int yMax)
        {
            XMax = xMax;
            YMax = yMax;
            CityPositions = new Collection<Collection<CityPosition>>();

            for (int row = 0; row < XMax; row++)
            {
                Collection<CityPosition> collection = new Collection<CityPosition>();
                for (int col = 0; col < YMax; col++)
                {
                    CityPosition cityPosition = new CityPosition(row, col, this);
                    collection.Add(cityPosition);
                    Console.Write(" " + cityPosition.XPos + "," + cityPosition.YPos + " ");
                }
                CityPositions.Add(collection);
                collection = new Collection<CityPosition>();
                Console.WriteLine("\n");
            }

            BuildMap();
        }

        public CityPosition GetCityPosition(int cityPositionX, int cityPositionY)
        {
            return CityPositions[cityPositionX][cityPositionY];
        }

        public CityPosition GetRandomEmptyCityPosition(Random random)
        {
            CityPosition cityPosition;
            while (true)
            {
                cityPosition = CityPositions[random.Next(XMax - 1)][random.Next(YMax - 1)];
                if (cityPosition.Building == null && cityPosition.Car == null && cityPosition.Passenger == null)
                {
                    return cityPosition;
                }
            }
        }

        public void UpdateCarCityPosition(Car car, int newCityXPosition, int newCityYPosition)
        {
            if (CityPositions[newCityXPosition][newCityYPosition].Building == null && CityPositions[newCityXPosition][newCityYPosition].Marked == false)
            {
                CityPositions[car.CityPosition.XPos][car.CityPosition.YPos].Car = null;
                CityPositions[newCityXPosition][newCityYPosition].Car = car;
                car.CityPosition = CityPositions[newCityXPosition][newCityYPosition];
                car.CityPosition.City.BuildMap(); 
            }
            else if (car.MovingAroundBuilding)
            {
                throw new ApplicationException();
            }
            else
            {
                car.MovingAroundBuilding = true;
                car.CityPosition.Marked = true;
                throw new Exception();
            }
        }

        public void UpdatePassengerCityPosition(Car car, Passenger passenger, int newCityXPosition, int newCityYPosition)
        {
            if (CityPositions[newCityXPosition][newCityYPosition].Building == null && CityPositions[newCityXPosition][newCityYPosition].Marked == false)
            {
                CityPositions[passenger.CityPosition.XPos][passenger.CityPosition.YPos].Passenger = null;
                CityPositions[newCityXPosition][newCityYPosition].Passenger = passenger;
                passenger.CityPosition = CityPositions[newCityXPosition][newCityYPosition];
                UpdateCarCityPosition(car, newCityXPosition, newCityYPosition);
            }
            else if (car.MovingAroundBuilding)
            {
                throw new ApplicationException();
            }
            else
            {
                car.MovingAroundBuilding = true;
                car.CityPosition.Marked = true;
                throw new Exception();
            }
        }
        
        public Building AddBuildingToCity(Random random)
        {
            CityPosition cityPosition = GetRandomEmptyCityPosition(random);
            Building building = new Building(cityPosition);
            cityPosition.Building = building;
            BuildMap();
            return building;
        }

        public Passenger AddPassengerToCity(Random random)
        {
            CityPosition cityPosition = GetRandomEmptyCityPosition(random);
            CityPosition destination = GetRandomEmptyCityPosition(random);

            Passenger passenger = new Passenger(destination.XPos, destination.YPos, cityPosition);
            cityPosition.Passenger = passenger;

            BuildMap();
            return passenger;
        }

        public void AddCarToCity (Car car)
        {
            CityPosition cityPosition = CityPositions[car.CityPosition.XPos][car.CityPosition.YPos];
            cityPosition.Car = car;
        }
        
        public void BuildMap()
        {
            Console.Clear();
            foreach (Collection<CityPosition> cityPositionCollections in CityPositions)
            {
                foreach (CityPosition cityPosition in cityPositionCollections)
                {
                    if (cityPosition.Car != null && cityPosition.Passenger != null && cityPosition.Car.Passenger != null && cityPosition.Passenger.Car != null)
                    {
                        Console.BackgroundColor = ConsoleColor.Cyan;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write(" " + "C+P" + " ");
                        Console.ResetColor();
                    }
                    else if (cityPosition.Car != null && cityPosition.Passenger != null)
                    {
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write(" " + "C");
                        Console.BackgroundColor = ConsoleColor.Cyan;
                        Console.Write(" ");
                        Console.ResetColor();
                        Console.BackgroundColor = ConsoleColor.Blue;
                        Console.Write("P" + " ");
                        Console.ResetColor();
                    }
                    else if (cityPosition.Car != null)
                    {
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write(" " + "Car" + " ");
                        Console.ResetColor();
                    }
                    else if (cityPosition.Passenger != null)
                    {
                        Console.BackgroundColor = ConsoleColor.Blue;
                        Console.Write(" " + "Psg" + " ");
                        Console.ResetColor();
                    }
                    else if (cityPosition.Building != null)
                    {
                        Console.BackgroundColor = ConsoleColor.Yellow;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write(" " + "Blg" + " ");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.Write(" " + cityPosition.XPos + "," + cityPosition.YPos + " ");
                    }
                }
                Console.WriteLine("\n");
            }
        }
    }
}