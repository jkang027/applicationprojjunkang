﻿using System;

namespace TransportProj.Domain
{
    public class Sedan : Car
    {
        public Sedan(CityPosition cityPosition) : base(cityPosition)
        {
            Speed = 1;
        }
        
        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Sedan moved to ({0}, {1})", CityPosition.XPos, CityPosition.YPos));
        }
    }
}
