﻿using System.Collections.Generic;

namespace TransportProj.Domain
{
    public class CityPosition
    {
        public int XPos { get; set; }
        public int YPos { get; set; }
        public City City { get; set; }
        public Car Car { get; set; }
        public Passenger Passenger { get; set; }
        public Building Building { get; set; }
        public bool Marked { get; set; }

        public CityPosition(int xPos, int yPos, City city)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Car = null;
            Passenger = null;
            Building = null;
            Marked = false;
        }

        public void RemovePassengerFromCity(Passenger passenger)
        {
            Passenger = null;
            passenger.CityPosition.City.BuildMap();
            passenger = null;
        }
    }
}
