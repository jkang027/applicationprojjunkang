﻿using System;
using System.Collections.ObjectModel;

namespace TransportProj.Domain
{
    public abstract class Car
    {
        public int Speed { get; set; }
        public Passenger Passenger { get; private set; }
        public CityPosition CityPosition { get; set; }
        public bool MovingAroundBuilding { get; set; }
        
        public Car(CityPosition cityPosition)
        {
            CityPosition = cityPosition;
            Passenger = null;
            MovingAroundBuilding = false;
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Car moving to ({0}, {1})", CityPosition.XPos, CityPosition.YPos));
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;

            foreach (Collection<CityPosition> cityPositionCollection in CityPosition.City.CityPositions)
            {
                foreach (CityPosition eachCityPosition in cityPositionCollection)
                {
                    eachCityPosition.Marked = false;
                }
            }
        }

        public void DropOffPassenger()
        {
            Passenger = null;

            foreach (Collection<CityPosition> cityPositionCollection in CityPosition.City.CityPositions)
            {
                foreach (CityPosition eachCityPosition in cityPositionCollection)
                {
                    eachCityPosition.Marked = false;
                }
            }
        }

        public void MoveUp(Passenger passenger)
        {
            if (CityPosition.YPos < City.YMax)
            {
                int moveDistance = MovingAroundBuilding ? 1 : Speed;
                for (int i = 0; i < moveDistance; i++)
                {
                    int newCityXPos = CityPosition.XPos;
                    if (Passenger == null)
                    {
                        int newCityYPos = CityPosition.YPos + 1;
                        try
                        {
                            CityPosition.City.UpdateCarCityPosition(this, newCityXPos, newCityYPos);
                        }
                        catch (ApplicationException e)
                        {
                            throw e;
                        }
                        catch (Exception)
                        {
                            GoAroundBuilding(passenger);
                        }
                        if (CityPosition.YPos == passenger.CityPosition.YPos) { break; }
                    }
                    else
                    {
                        int newCityYPos = CityPosition.YPos + 1;
                        try
                        {
                            CityPosition.City.UpdatePassengerCityPosition(this, passenger, newCityXPos, newCityYPos);
                        }
                        catch (ApplicationException e)
                        {
                            throw e;
                        }
                        catch (Exception)
                        {
                            GoAroundBuilding(passenger);
                        }
                        if (CityPosition.YPos == passenger.DestinationYPos) { break; }
                    }
                    System.Threading.Thread.Sleep(250);
                }
                if (!MovingAroundBuilding)
                {
                    WritePositionToConsole();
                }
                MovingAroundBuilding = false; 
            }
        }

        public void MoveDown(Passenger passenger)
        {
            if (CityPosition.YPos > 0)
            {
                int moveDistance = MovingAroundBuilding ? 1 : Speed;
                for (int i = 0; i < moveDistance; i++)
                {
                    int newCityXPos = CityPosition.XPos;
                    if (Passenger == null)
                    {
                        int newCityYPos = CityPosition.YPos - 1;
                        try
                        {
                            CityPosition.City.UpdateCarCityPosition(this, newCityXPos, newCityYPos);
                        }
                        catch (ApplicationException e)
                        {
                            throw e;
                        }
                        catch (Exception)
                        {
                            GoAroundBuilding(passenger);
                        }
                        if (CityPosition.YPos == passenger.CityPosition.YPos) { break; }
                    }
                    else
                    {
                        int newCityYPos = CityPosition.YPos - 1;
                        try
                        {
                            CityPosition.City.UpdatePassengerCityPosition(this, passenger, newCityXPos, newCityYPos);
                        }
                        catch (ApplicationException e)
                        {
                            throw e;
                        }
                        catch (Exception)
                        {
                            GoAroundBuilding(passenger);
                        }
                        if (CityPosition.YPos == passenger.DestinationYPos) { break; }
                    }
                    System.Threading.Thread.Sleep(250);
                }
                if (!MovingAroundBuilding)
                {
                    WritePositionToConsole();
                }
                MovingAroundBuilding = false;
            }
        }

        public void MoveRight(Passenger passenger)
        {
            if (CityPosition.XPos < City.XMax)
            {
                int moveDistance = MovingAroundBuilding ? 1 : Speed;
                for (int i = 0; i < moveDistance; i++)
                {
                    int newCityYPos = CityPosition.YPos;
                    if (Passenger == null)
                    {
                        int newCityXPos = CityPosition.XPos + 1;
                        try
                        {
                            CityPosition.City.UpdateCarCityPosition(this, newCityXPos, newCityYPos);
                        }
                        catch (ApplicationException e)
                        {
                            throw e;
                        }
                        catch (Exception)
                        {
                            GoAroundBuilding(passenger);
                        }
                        if (CityPosition.XPos == passenger.CityPosition.XPos) { break; }
                    }
                    else
                    {
                        int newCityXPos = CityPosition.XPos + 1;
                        try
                        {
                            CityPosition.City.UpdatePassengerCityPosition(this, passenger, newCityXPos, newCityYPos);
                        }
                        catch (ApplicationException e)
                        {
                            throw e;
                        }
                        catch (Exception)
                        {
                            GoAroundBuilding(passenger);
                        }
                        if (CityPosition.XPos == passenger.DestinationXPos) { break; }
                    }
                    System.Threading.Thread.Sleep(250);
                }
                if (!MovingAroundBuilding)
                {
                    WritePositionToConsole();
                }
                MovingAroundBuilding = false;
            }
        }

        public void MoveLeft(Passenger passenger)
        {
            if (CityPosition.XPos > 0)
            {
                int moveDistance = MovingAroundBuilding ? 1 : Speed;
                for (int i = 0; i < moveDistance; i++)
                {
                    int newCityYPos = CityPosition.YPos;
                    if (Passenger == null)
                    {
                        int newCityXPos = CityPosition.XPos - 1;
                        try
                        {
                            CityPosition.City.UpdateCarCityPosition(this, newCityXPos, newCityYPos);
                        }
                        catch (ApplicationException e)
                        {
                            throw e;
                        }
                        catch (Exception)
                        {
                            GoAroundBuilding(passenger);
                        }
                        if (CityPosition.XPos == passenger.CityPosition.XPos) { break; }
                    }
                    else
                    {
                        int newCityXPos = CityPosition.XPos - 1;
                        try
                        {
                            CityPosition.City.UpdatePassengerCityPosition(this, passenger, newCityXPos, newCityYPos);
                        }
                        catch (ApplicationException e)
                        {
                            throw e;
                        }
                        catch (Exception)
                        {
                            GoAroundBuilding(passenger);
                        }
                        if (CityPosition.XPos == passenger.DestinationXPos) { break; }
                    }
                    System.Threading.Thread.Sleep(250);
                }
                if (!MovingAroundBuilding)
                {
                    WritePositionToConsole();
                }
                MovingAroundBuilding = false;
            }
        }

        //TODO: This method does not work 100% of cases. It tends to break if the car runs into multiple buildings right after eachother.
        public void GoAroundBuilding(Passenger passenger)
        {
            if (Passenger == null)
            {
                if (passenger.CityPosition.XPos > CityPosition.XPos)
                {
                    if (passenger.CityPosition.YPos > CityPosition.YPos)
                    {
                        try
                        {
                            MoveUp(passenger);
                        }
                        catch
                        {
                            try
                            {
                                MoveDown(passenger);
                            }
                            catch
                            {
                                MoveLeft(passenger);
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            MoveDown(passenger);
                        }
                        catch
                        {
                            try
                            {
                                MoveUp(passenger);
                            }
                            catch
                            {
                                MoveLeft(passenger);
                            }
                        }
                    }
                }
                else if (passenger.CityPosition.XPos < CityPosition.XPos)
                {
                    if (passenger.CityPosition.YPos > CityPosition.YPos)
                    {
                        try
                        {
                            MoveUp(passenger);
                        }
                        catch
                        {
                            try
                            {
                                MoveDown(passenger);
                            }
                            catch
                            {
                                MoveRight(passenger);
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            MoveDown(passenger);
                        }
                        catch
                        {
                            try
                            {
                                MoveUp(passenger);
                            }
                            catch
                            {
                                MoveRight(passenger);
                            }
                        }

                    }
                }
                else if (passenger.CityPosition.YPos > CityPosition.YPos)
                {
                    try
                    {
                        MoveRight(passenger);
                    }
                    catch
                    {
                        try
                        {
                            MoveLeft(passenger);
                        }
                        catch
                        {
                            MoveDown(passenger);
                        }
                    }
                }
                else
                {
                    try
                    {
                        MoveRight(passenger);
                    }
                    catch
                    {
                        try
                        {
                            MoveLeft(passenger);
                        }
                        catch
                        {
                            MoveUp(passenger);
                        }
                    }
                }
            }
            else
            {
                if (passenger.DestinationXPos > CityPosition.XPos)
                {
                    if (passenger.DestinationYPos > CityPosition.YPos)
                    {
                        try
                        {
                            MoveUp(passenger);
                        }
                        catch
                        {
                            try
                            {
                                MoveDown(passenger);
                            }
                            catch
                            {
                                MoveLeft(passenger);
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            MoveDown(passenger);
                        }
                        catch
                        {
                            try
                            {
                                MoveUp(passenger);
                            }
                            catch
                            {
                                MoveLeft(passenger);
                            }
                        }
                    }
                }
                else if (passenger.DestinationXPos < CityPosition.XPos)
                {
                    if (passenger.DestinationYPos > CityPosition.YPos)
                    {
                        try
                        {
                            MoveUp(passenger);
                        }
                        catch
                        {
                            try
                            {
                                MoveDown(passenger);
                            }
                            catch
                            {
                                MoveRight(passenger);
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            MoveDown(passenger);
                        }
                        catch
                        {
                            try
                            {
                                MoveUp(passenger);
                            }
                            catch
                            {
                                MoveRight(passenger);
                            }
                        }
                    }
                }
                else if (passenger.DestinationYPos > CityPosition.YPos)
                {
                    try
                    {
                        MoveRight(passenger);
                    }
                    catch
                    {
                        try
                        {
                            MoveLeft(passenger);
                        }
                        catch
                        {
                            MoveDown(passenger);
                        }
                    }
                }
                else
                {
                    try
                    {
                        MoveRight(passenger);
                    }
                    catch
                    {
                        try
                        {
                            MoveLeft(passenger);
                        }
                        catch
                        {
                            MoveUp(passenger);
                        }
                    }
                }
            }
        }
    }
}
