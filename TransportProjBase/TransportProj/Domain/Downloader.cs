﻿using System.Net;
using System.Threading.Tasks;

namespace TransportProj.Domain
{
    public class Downloader
    {
        public async Task<string> Download()
        {
            using (WebClient wc = new WebClient())
            {
                return await wc.DownloadStringTaskAsync("https://www.veyo.com/");
            }
        }
    }
}
