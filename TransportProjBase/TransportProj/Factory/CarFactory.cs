﻿using TransportProj.Domain;

namespace TransportProj.Factory
{
    public abstract class CarFactory
    {
        public abstract Car createCar(string type, CityPosition cityPosition);
    }
}
