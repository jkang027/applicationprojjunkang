﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportProj.Domain;

namespace TransportProj.Factory
{
    public class HondaFactory : CarFactory
    {
        public override Car createCar(string type, CityPosition cityPosition)
        {
            switch (type)
            {
                case "sedan":
                    return new Sedan(cityPosition);
                case "race car":
                    return new RaceCar(cityPosition);
                case "super car":
                    return new SuperCar(cityPosition);
                default:
                    throw new NotSupportedException();
            }
        }
    }
}
