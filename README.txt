Commit 12: "enhancing GoAroundBuilding method in Car.cs"

I was able to find a little bit of time to work on this project, even though I turned in the project last night.
Changed City.cs UpdateCarCityPosition and UpdatePassengerCityPosition methods. If the car/passenger can't move to the new position, and if the car is already trying to move around a building, it will throw a different kind of exception. This way, the exception can be thrown to the GoAroundBuilding method in Car.cs.
Changed Car.cs Whenever PickupPassenger or DropOffPassenger methods are called, all city positions will become unmarked.
In my last commit, I said the GoAroundBuilding method works ~80% of the time. This was probably incorrect and too high. It worked closer to ~60% of the time. Now it should work around ~80% of the time (these are just rough guesses). Will continue trying to make it work properly in all scenarios.

Commit 11: "continue implementing cars moving around buildings"

Changed Car.cs
    Car constructor no longer requires passenger. When a car is instantiated, passenger always starts as null anyway.
    Added MovingAroundBuilding property to Car.cs. If a car needs to move around a building, this will be set to true. If a car successfully moves to any position, it will be reset to false.
    Implemented GoAroundBuilding method. This method is not 100%. It works ~60% of the time. The method has problems when there are multiple buildings to go around immediately after eachother. Will look into a better fix and better implementation of this method.
Added Marked property to CityPosition.cs so a car will know whether or not it has been at this cityposition before while transporting the passenger. 
Changed CarFactory.cs *This was incorrect. The only things changed in CarFactory and HondaFactory was HondaFactory now uses a switch statement.*
Changed City.cs
    Added GetRandomEmptyCityPosition method to City.cs. Will now return a cityposition in the city that does not have a building, passenger, or car already in it. It also takes in an instance of Random because if there are multiple instances of Random getting random numbers at the same time, they will produce the same series of random numbers.
    AddBuildingToCity and AddPassengerToCity methods changed to implement above method. 
Changed Passenger.cs constructor to always set Arrived property to false when a new passenger is instantiated. Not sure why I had it require bool arrived input.


Commit 10: "start implementing cars moving around buildings"

Changed City.cs. UpdateCarCityPosition and UpdatePassengerCityPosition changed to throw exception if car/passenger attempt to update to a new citylocation that has a building in it.
Car MoveUp, Down, Left, Right methods changed to move one position each time but the updatecityposition methods will be run the car's Speed # of times. If the car reaches its desired x or y but has extra speed leftover to update again, it will break out of the loop.
Refactored Tick method in Program.cs.
GoAroundBuilding method in Car.cs is ready to be implemented, and I've been working on it for a while now, but haven't thought of a good way to implement it. My test GoAroundBuilding method was only making cars go around buildings ~50% of the time and it couldn't really be enhanced so I deleted it.
Cars, buildings, passengers, passenger destinations, etc can be added to the same cityposition on the city map, occasionally making it impossible for a car to pick up or drop off a passenger. Will look into this issue.

Commit 9: "refactored several minor details"

Changed City.cs 
    AddCityPositionToCity method removed. No longer necessary.
    City.cs constructor will now create all the new citypositions whenever a new City is instantiated.
    UpdateCarCityPosition and UpdatePassengerCityPosition methods. They now call the BuildMap method. They also don't need int oldCityXPosition or oldCityYPosition.
Changed Car.cs MoveUp, Down, Left, Right methods to implement above change. Also removed input City. It was not needed.
Moved BuildMap method from Program.cs to City.cs. Thought it made more sense to be a City method.
Changed Passenger.cs removed GetCurrentXPos, GetCurrentYPos methods. No longer needed with cityposition implementation.
Having cars move around buildings in the city has not been implemented yet.

Commit 8: "finish implementing CityPosition class to build a 2d city map"

As I started updating RaceCar.cs and Sedan.cs MoveUp, Down, Left, Right methods to utilize CityPosition, I realized It was a lot of repetition. The only difference was the distance the car moved, so Added a speed property to all cars and created concrete MoveUp, Down, Left, Right methods implementing the car's speed.
Changed Car.cs to have Speed property.
Changed Car.cs MoveUp, Down, Left, Right methods to be concrete utilizing Speed property.
Now that Cars and passengers dont have their own XPos and YPos and are using the CityPosition class, had to change how they moved in the MoveUp, Down, Left, Right methods in Car.cs.
Added UpdateCarCityPosition and UpdatePassengerCityPosition methods to City.cs for use in Car.cs's MoveUp, Down, Left, Right methods.
Added SuperCar.cs to Domain. Added SuperCar to HondaFactory.
Having cars move around buildings in the city has not been implemented yet.

Commit 7: "start implementing CityPosition class to build a 2d city map"

Changed CityPositions.cs to have XPos, YPos, City, Car, Passenger, and Building properties.
Changed City.cs to have CityPositions property. Removed Cars, Passengers, and Buildings properties.
Changed Car.cs. Removed XPos and YPos properties and added CityPosition property.
Changed Building.cs. Removed XPos and YPos properties and added CityPosition property.
Changed Passenger.cs. Removed StartingXPos and StartingYPos properties and added CityPosition property.
Updated above classes constructors/methods as needed to implement CityPositions properties.
Added BuildMap method to Program.cs to build the 2d map. 
    BuildMap method is not bug free and is not fully functional yet.
2d city map implementation not complete.
    Car and Passenger's CityPositions do not update when moving yet.
    CityPositions' Car and Passenger properties do not update when Car/Passenger move out of them yet.
    Having cars move around buildings in the city has not been implemented yet.

Commit 6: "implemented factory pattern"

Created a Factory folder.
Created CarFactory abstract class and HondaFactory.cs concrete class.
I feel like implementing the factory pattern was a little pointless in this scenario since there is only one type of factory (Honda) which will create sedans or race cars.
Changed how the AddCarToCity method works in City.cs. The factory will now create the car, and the method will be called to add that new car to the city.

Commit 5: "implemented creating buildings in the city. cars dont move around buildings yet."

I noticed there was a bug that seldom occured when a racecar would move 2 spaces when it should have only moved one. Fixed that issue by changing the RaceCar.cs MoveUp, Down, Left, Right methods' conditional statements.
Created a new class called Building.cs.
Added AddBuildingToCity method to City.cs.
Added Cars, Passengers, and Buildings ICollections to City class.
Updated City constructor in City.cs for those changes.
Having cars move around buildings in the city has not been implemented yet.

Commit 4: "refactored the Tick method"

Since the Tick method I wrote had so many if and else if statements, I thought I would refactor it to look a bit cleaner as a switch statement. I know some people don't like ternary statements because they can be harder to read and understand, so I left the original Tick method commented down below the new Tick method.

Commit 3: "implemented download veyo website when moving"

I installed the nuget package Nito.AsyncEx.
Created a new class called Downloader.cs to implement downloading veyo.com async.
Downloader.cs has a method called Download which uses a new WebClient to download veyo.com to a string and return that string.
I also moved carChoice (when person picks a car) into a while loop because with an invalid input, it was still continuing the program and breaking.
I also fixed the namespaces in the classes in Domain because I forgot to fix them when I moved them.

Commit 2: "implemented racecar"

I created the RaceCar class.
In the Car class, I changed the MoveUp, Down, Left, and Right methods to take in the passenger as input so that the racecar would know whether to move 2 spaces or just 1. 

Commit 1: "Tick method implemented. Added a few properties/methods to Car.cs and Passenger.cs."

Moved classes to Domain folder.
I implemented the Tick method.
I added a few properties and methods to Car.cs and Passenger.cs.
Car.cs
    Added "DropOffPassenger" method to Car.cs.
Passenger.cs
    Added bool "Arrived" property to passenger to indicate whether the passenger had been taken to destination already.
    Added "Arrived" property to passenger constructor and City's "AddPassengerToCity" method.